package co.za.mamamoney.cucumber;

import co.za.mamamoney.CustomerExperiencePortalApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = CustomerExperiencePortalApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
