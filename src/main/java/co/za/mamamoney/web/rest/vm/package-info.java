/**
 * View Models used by Spring MVC REST controllers.
 */
package co.za.mamamoney.web.rest.vm;
