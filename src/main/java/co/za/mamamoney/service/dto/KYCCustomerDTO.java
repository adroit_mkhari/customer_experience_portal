package co.za.mamamoney.service.dto;

import co.za.mamamoney.domain.enumeration.KYCSTATUS;
import io.swagger.annotations.ApiModel;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * A DTO for the KYC Customer entity.
 */
@ApiModel(description = "The Request entity.")
public class KYCCustomerDTO implements Serializable {

    private Long id;

    @NotNull
    @Pattern(
        regexp = "(((\\d{2}((0[13578]|1[02])(0[1-9]|[12]\\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\\d|30)|02(0[1-9]|1\\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\\d{4})( |-)(\\d{3})|(\\d{7}))"
    )
    private String idNumber;

    private KYCSTATUS status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public KYCSTATUS getStatus() {
        return status;
    }

    public void setStatus(KYCSTATUS status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof KYCCustomerDTO)) {
            return false;
        }

        KYCCustomerDTO kycCustomerDTO = (KYCCustomerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, kycCustomerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerDTO{" +
            "id=" + getId() +
            ", idNumber='" + getIdNumber() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
