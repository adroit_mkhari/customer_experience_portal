package co.za.mamamoney.domain.enumeration;

/**
 * The STATUS enumeration.
 */
public enum KYCSTATUS {
    Y,
    N,
}
