import { STATUS } from 'app/entities/enumerations/status.model';

export interface ICustomer {
  id?: number;
  firstName?: string;
  lastName?: string;
  cellPhoneNumber?: string;
  idNumber?: string;
  status?: STATUS | null;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public cellPhoneNumber?: string,
    public idNumber?: string,
    public status?: STATUS | null
  ) {}
}

export function getCustomerIdentifier(customer: ICustomer): number | undefined {
  return customer.id;
}
